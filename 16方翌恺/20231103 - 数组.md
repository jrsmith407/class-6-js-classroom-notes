## 数组
- 包含任意数据类型
- 创建数组方法
1. 常规方式 
```js
var myCars=new Array();
myCars[0]="Saab";      
myCars[1]="Volvo";
myCars[2]="BMW";
```
2. 简洁方式
```js
var myCars=new Array("Saab","Volvo","BMW");
```
- length：
    - 取得Array的长度
- indexOf：
    - 搜索一个指定的元素的位置
- slice：
    - arr.slice(0,3):从索引0开始到索引3结束，但不包含索引3

- push 和 pop
    - push：在数组末尾添加若干元素
    - pop：把Array最后一个元素删除掉

- unshift和shift
    - unshift：往头部添加若干元素
    - shift：删除第一个元素

- sort：对当前Array排序
- reverse：对整个数组反转


### 对象
- JavaScript的对象是一种无序的集合数据类型，它由若干键值对组成

- 定义对象
```js
var xiaoming = {
    name: '小明',
    birth: 1990,
    school: 'No.1 Middle School',
    height: 1.70,
    weight: 65,
    score: null
};
```

### 判断

#### if...else
```js
var age = 20;
if (age >= 18) { // 如果age >= 18为true，则执行if语句块
    alert('adult');
} else { // 否则执行else语句块
    alert('teenager');
}
```

### 循环
- for循环：通过初始条件、结束条件和递增条件来循环执行语句块