## 搭建服务器
1. win+r：输入cmd

ssh root@域名 输入密码
2. 安装 这个:apt install nginx

3. 输入:cd /var/www

4. 创建一个文件夹： mkdir 文件夹名称(有用)

5. 创建index.html 输入好代码，按ctrl+esc下面的键

6. 输入scp .\index.html root@域名:/var/www/自己创的文件夹名称

7. 输入 cd /etc/nginx/conf.d vim 域名.conf

i启用编辑，esc退出编辑，:wq保存

8. 输入
```
server {
  listen 80;
  server_name 域名;

  location / {
      root /var/www/自己创建的文件夹名称;
      index index.html;
  }
}
```
9. 在创建的文件夹输入 cat index.html 可以查看提交的html文档

10. nginx -s reload //这个命令用于在不重启nginx的情况下，重新加载配置文件（以让新的配置文件生效，一般用于确认配置文件没有问题后执行）