## Promise
在js中，所有代码都是单线程执行的，导致js的所有网络操作，浏览器事件，都必须是异步执行，异步执行可以用回调函数实现。

|状态|描述|
|-|-|
|待定（pending）|初始状态，既没有被兑现，也没有被拒绝|
|已兑现（fulfilled）|意味着操作成功完成|
|已拒绝（rejected）|意味着操作失败|


### .then()
.then():方法最多接受两个参数
 - 第一个参数是Promise兑现时的回调函数
 - 第二个是Promise拒绝时的回调函数

每一个.then()返回一个新生的Promise对象可被用于链式调用

### Promise并发
Promise类提供四个静态方法来促进异步任务的并发：

Promise.all():在所有传入的Promise都被兑现；在任意一个Promise被拒绝时拒绝

Promise.allSrttled():在所有的Promise都被敲定时兑现

Promise.any():在任意一个Promise被兑现时兑现；仅在所有的Promise都被拒绝时才会拒绝

Promise.race():在任意一个Promise被敲定时敲定。换句话说，在任意一个Promise被兑换时兑现；在任意一个的Promise被拒绝时拒绝



### 并行 异行任务的同时调用
 1. 所有任务需要都完成，任务才算完成，才能继续做某事。

 2. 所有任务里只要有一个先完成，那就算完成，可以继续做某事。