## 查找
### 用find()查找：

```js
var ul = $('ul.lang'); // 获得<ul>
var dy = ul.find('.dy'); // 获得JavaScript, Python, Scheme
var swf = ul.find('#swift'); // 获得Swift
var hsk = ul.find('[name=haskell]'); // 获得Haskell
```

### 如果要从当前节点开始向上查找，使用parent()方法：

```js
var swf = $('#swift'); // 获得Swift
var parent = swf.parent(); // 获得Swift的上层节点<ul>
var a = swf.parent('.red'); // 获得Swift的上层节点<ul>，同时传入过滤条件。如果ul不符合条件，返回空jQuery对象
```

### 对于位于同一层级的节点，可以通过next()和prev()方法，例如：

当我们已经拿到Swift节点后：

```js
var swift = $('#swift');

swift.next(); // Scheme
swift.next('[name=haskell]'); // 空的jQuery对象，因为Swift的下一个元素Scheme不符合条件[name=haskell]

swift.prev(); // Python
swift.prev('.dy'); // Python，因为Python同时符合过滤器条件.dy
```

## 过滤
### filter()方法可以过滤掉不符合选择器条件的节点：

```js
var langs = $('ul.lang li'); // 拿到JavaScript, Python, Swift, Scheme和Haskell
var a = langs.filter('.dy'); // 拿到JavaScript, Python, Scheme
```

或者传入一个函数，要特别注意函数内部的this被绑定为DOM对象，不是jQuery对象：

```js
var langs = $('ul.lang li'); // 拿到JavaScript, Python, Swift, Scheme和Haskell
langs.filter(function () {
    return this.innerHTML.indexOf('S') === 0; // 返回S开头的节点
}); // 拿到Swift, Scheme
```

### map()方法把一个jQuery对象包含的若干DOM节点转化为其他对象：

```js
var langs = $('ul.lang li'); // 拿到JavaScript, Python, Swift, Scheme和Haskell
var arr = langs.map(function () {
    return this.innerHTML;
}).get(); // 用get()拿到包含string的Array：['JavaScript', 'Python', 'Swift', 'Scheme', 'Haskell']
```

此外，一个jQuery对象如果包含了不止一个DOM节点
### first()、last()和slice()方法可以返回一个新的jQuery对象
把不需要的DOM节点去掉：

```js
var langs = $('ul.lang li'); // 拿到JavaScript, Python, Swift, Scheme和Haskell
var js = langs.first(); // JavaScript，相当于$('ul.lang li:first-child')
var haskell = langs.last(); // Haskell, 相当于$('ul.lang li:last-child')
var sub = langs.slice(2, 4); // Swift, Scheme, 参数和数组的slice()方法一致
```

## jQuery能够绑定的事件
### 鼠标事件
  - click：鼠标单击时触发
  - dblclick:鼠标双击时触发
  - mouseenter：鼠标进入时触发
  - mouseleave：鼠标移出时触发
  - mousemove：鼠标在DOM内部移动时触发
  - hover：鼠标进入和退出时触发两个函数，相当于mouseenter加上mouseleave

### 键盘事件
键盘事件仅作用在当前焦点的DOM上，通常是`<input>`和`<textarea>`
  - keydown:键盘按下时触发
  - keyup:键盘松开时触发
  - keypress:按一次键后触发
其他事件
  - focus:当DOM获得焦点时触发
  - blur:当DOM失去焦点时触发
  - change:当`<input>、<select>`和`<textarea>`的内容改变时触发
  - submit：当`<form>`提交时被触发
  - ready:当页面被载入并且DOM树完成初始化后触发


### 取消绑定
可以通过off('click',function)来实现
```js
function hello() {
    alert('hello!');
}

a.click(hello); // 绑定事件

// 10秒钟后解除绑定:
setTimeout(function () {
    a.off('click', hello);
}, 10000);
```